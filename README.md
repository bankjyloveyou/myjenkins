# course-jenkins

**Multi Pipeline**

คือ การสร้าง Job ตามจำนวน branch ใน source control ที่ระบุ 
โดยจะอ่านการทำงานจาก Jenkinfiles ของแต่ละ branch


**Lab**

1. ทดลองสร้าง Multi Pipeline เพื่อ download sourcecode
 > ```https://gitlab.com/isnoopy/course-jenkins```
2. แสดงรายการไฟล์
3. แสดงเนื้อหาไฟล์
> ```file.txt```
